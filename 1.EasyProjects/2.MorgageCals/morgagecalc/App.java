import java.text.NumberFormat;
import java.util.Scanner;

public class App 
{
    public static void main(String[] args)
    {
        //scanner for input 
        Scanner scanner = new Scanner(System.in);

        //stores loan amount 
        System.out.println("Enter Loan amount: ");
        int loanAmount = scanner.nextInt();

        //stores terms in years
        System.out.println("Enter the loan terms (Years): ");
        int termInYears = scanner.nextInt();

        //stores interests rate of laon
        System.out.println("Enter interest rate: ");
        int interestRate = scanner.nextShort();

        //Display details of the loan 
        double monthlyPayment=calculations(
                loanAmount,
                termInYears,
                interestRate
            );

        //Formatting     
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        NumberFormat interestFormat = NumberFormat.getPercentInstance();

        //Displaying details of the loan
        System.out.println("Loan Amount: " + currencyFormat.format(loanAmount));
        System.out.println("Loan Term: " + termInYears + " Years");
        System.out.println("Monthly Payment: " + currencyFormat.format(monthlyPayment));

    }

    public static double calculations(int loanAmount, int termInYears, double interestRate)
    {
        //converts interest rates in to usable format
        interestRate /= 100.0;

        //divides interest rate in to monthly rate 
        double monthlyRate = interestRate / 12;

        //The length of the term in months 
        int termsInMonths = termInYears * 12;

        //calculate the monthly payment 
        double monthlyPayment = 
        (loanAmount*monthlyRate) /
        (1-Math.pow(1+monthlyRate, -termsInMonths));

        //returns previously calculated
        return monthlyPayment;

    }
}